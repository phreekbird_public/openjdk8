FROM phreekbird/core:latest
MAINTAINER phreekbird webmaster@phreekbird.net
# run apt-get update to refresh package list, and imidiatly run installs, to pull latest updates.
# grab the software-properties-common package
RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install software-properties-common -y
# use that above package to get a new PPA repo for openjdk-8
RUN DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:openjdk-r/ppa -y
# run another apt-get update to refrsh package list, and pull a copy of java.
RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install openjdk-8-jre -y
# setup the required java variables, we will need these later on for all the things.
ENV JSVC /usr/bin/jsv
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
